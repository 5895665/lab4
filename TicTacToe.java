import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

        printBoard(board);
        int i;
        for ( i = 0; i < 4;i++) {

            System.out.print("Player 1 enter row number:");
            int row = reader.nextInt();
            System.out.print("Player 1 enter column number:");
            int col = reader.nextInt();


            while(row > 3 || col > 3){
                System.out.println("You have entered an invalid number. Please enter one of the numbers 1, 2, 3.");
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();

            }


            if (board[row - 1][col - 1] == ' ') {
                board[row - 1][col - 1] = 'X';
                printBoard(board);
            } else if(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){
                while(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){
                    System.out.println("the field is full");
                    System.out.print("Player 1 enter row number:");
                    row = reader.nextInt();
                    System.out.print("Player 1 enter column number:");
                    col = reader.nextInt();
                    while(row > 3 || col > 3) {
                        System.out.println("You have entered an invalid number. Please enter one of the numbers 1, 2, 3.");
                        System.out.print("Player 1 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 1 enter column number:");
                        col = reader.nextInt();
                    }
                }board[row - 1][col - 1] = 'X';
                printBoard(board);


            }
            boolean checkBoard1 = true;
            if (board[0][2] == 'X' && board[0][0] == 'X' && board[0][1] == 'X') {
                checkBoard1 = false;
            } else if (board[1][2] == 'X' && board[1][0] == 'X' && board[1][1] == 'X' ) {
                checkBoard1 = false;
            } else if (board[2][2] == 'X' && board[2][0] == 'X' && board[2][1] == 'X') {
                checkBoard1 = false;
            } else if (board[2][0] == 'X' && board[0][0] == 'X' && board[1][0] == 'X') {
                checkBoard1 = false;
            } else if (board[2][1] == 'X' && board[0][1] == 'X' && board[1][1] == 'X') {
                checkBoard1 = false;
            } else if (board[2][2] == 'X' && board[0][2] == 'X' && board[1][2] == 'X') {
                checkBoard1 = false;
            } else if (board[2][2] == 'X' && board[0][0] == 'X' && board[1][1] == 'X') {
                checkBoard1 = false;
            }
            if(checkBoard1){

            }else{
                System.out.println("Player 1 wins");
                break;
            }


            System.out.print("Player 2 enter row number:");
            row = reader.nextInt();
            System.out.print("Player 2 enter column number:");
            col = reader.nextInt();
            while(row > 3 || col > 3){
                System.out.println("You have entered an invalid number. Please enter one of the numbers 1, 2, 3.");
                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();

            }
            if (board[row - 1][col - 1] == ' ') {
                board[row - 1][col - 1] = 'O';
                printBoard(board);
            } else if(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){
                while(board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){
                    System.out.println("the field is full");
                    System.out.print("Player 2 enter row number:");
                    row = reader.nextInt();
                    System.out.print("Player 2 enter column number:");
                    col = reader.nextInt();
                    while(row > 3 || col > 3){
                        System.out.println("You have entered an invalid number. Please enter one of the numbers 1, 2, 3.");
                        System.out.print("Player 2 enter row number:");
                        row = reader.nextInt();
                        System.out.print("Player 2 enter column number:");
                        col = reader.nextInt();

                    }

                }board[row - 1][col - 1] = 'O';
                printBoard(board);


            }
            boolean checkBoard = true;
            if (board[0][2] == 'O' && board[0][0] == 'O' && board[0][1] == 'O') {
                checkBoard = false;
            } else if (board[1][2] == 'O' && board[1][0] == 'O' && board[1][1] == 'O' ) {
                checkBoard = false;
            } else if (board[2][2] == 'O' && board[2][0] == 'O' && board[2][1] == 'O') {
                checkBoard = false;
            } else if (board[2][0] == 'O' && board[0][0] == 'O' && board[1][0] == 'O') {
                checkBoard = false;
            } else if (board[2][1] == 'O' && board[0][1] == 'O' && board[1][1] == 'O') {
                checkBoard = false;
            } else if (board[2][2] == 'O' && board[0][2] == 'O' && board[1][2] == 'O') {
                checkBoard = false;
            } else if (board[2][2] == 'O' && board[0][0] == 'O' && board[1][1] == 'O') {
                checkBoard = false;
            }
            if(checkBoard){

            }else{
                System.out.println("Player 2 wins");
                break;
            }

        }if(i==4){
            System.out.print("Player 1 enter row number:");
            int row = reader.nextInt();
            System.out.print("Player 1 enter column number:");
            int col = reader.nextInt();


            if (board[row - 1][col - 1] == ' ') {
                board[row - 1][col - 1] = 'X';
                printBoard(board);
                System.out.println("the game ended in a draw.");
            }else if (board[row - 1][col - 1] == 'O' || board[row - 1][col - 1] == 'X'){
                System.out.println("the field is full");
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();
                board[row - 1][col - 1] = 'X';
                printBoard(board);
                System.out.println("the game ended in a draw.");
            }
        }

    }

    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");

        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");

        }
    }


}